<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "albumes".
 *
 * @property int $idalbum
 * @property string|null $titulo
 * @property string|null $genero
 *
 * @property Forman[] $formen
 * @property Canciones[] $idcancions
 */
class Albumes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'albumes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'genero'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idalbum' => 'Idalbum',
            'titulo' => 'Titulo',
            'genero' => 'Genero',
        ];
    }

    /**
     * Gets query for [[Formen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormen()
    {
        return $this->hasMany(Forman::className(), ['idalbum' => 'idalbum']);
    }

    /**
     * Gets query for [[Idcancions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancions()
    {
        return $this->hasMany(Canciones::className(), ['idcancion' => 'idcancion'])->viaTable('forman', ['idalbum' => 'idalbum']);
    }
}
