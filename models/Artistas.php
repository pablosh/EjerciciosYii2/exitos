<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artistas".
 *
 * @property int $idartista
 * @property string|null $nombre
 * @property int|null $idgrupo
 *
 * @property Canciones[] $idcancions
 * @property Grupos $idgrupo0
 * @property Lanzamientos[] $lanzamientos
 */
class Artistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idgrupo'], 'integer'],
            [['nombre'], 'string', 'max' => 80],
            [['idgrupo'], 'exist', 'skipOnError' => true, 'targetClass' => Grupos::className(), 'targetAttribute' => ['idgrupo' => 'idgrupo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idartista' => 'Idartista',
            'nombre' => 'Nombre',
            'idgrupo' => 'Idgrupo',
        ];
    }

    /**
     * Gets query for [[Idcancions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancions()
    {
        return $this->hasMany(Canciones::className(), ['idcancion' => 'idcancion'])->viaTable('lanzamientos', ['idartista' => 'idartista']);
    }

    /**
     * Gets query for [[Idgrupo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdgrupo0()
    {
        return $this->hasOne(Grupos::className(), ['idgrupo' => 'idgrupo']);
    }

    /**
     * Gets query for [[Lanzamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLanzamientos()
    {
        return $this->hasMany(Lanzamientos::className(), ['idartista' => 'idartista']);
    }
}
