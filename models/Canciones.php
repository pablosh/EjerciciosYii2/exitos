<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "canciones".
 *
 * @property int $idcancion
 * @property string|null $nombre
 * @property string|null $duracion
 * @property string|null $lanzamiento
 * @property int|null $reproducciones
 *
 * @property Forman[] $formen
 * @property Albumes[] $idalbums
 * @property Artistas[] $idartistas
 * @property Lanzamientos[] $lanzamientos
 */
class Canciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public $nomalbum;
    public $nomgrupo;
    public $nomartistas;

    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion', 'lanzamiento'], 'safe'],
            [['reproducciones'], 'integer'],
            [['nombre'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcancion' => 'Idcancion',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
            'lanzamiento' => 'Lanzamiento',
            'reproducciones' => 'Reproducciones',
            'nomalbum' => 'Álbum',
            'nomgrupo' => 'Grupo',
            'nomartistas' => 'Artistas'
        ];
    }

    /**
     * Gets query for [[Formen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormen()
    {
        return $this->hasMany(Forman::className(), ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idalbums]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdalbums()
    {
        return $this->hasMany(Albumes::className(), ['idalbum' => 'idalbum'])->viaTable('forman', ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idartistas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdartistas()
    {
        return $this->hasMany(Artistas::className(), ['idartista' => 'idartista'])->viaTable('lanzamientos', ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Lanzamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLanzamientos()
    {
        return $this->hasMany(Lanzamientos::className(), ['idcancion' => 'idcancion']);
    }
}
