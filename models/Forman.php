<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forman".
 *
 * @property int $idforma
 * @property int $idcancion
 * @property int $idalbum
 *
 * @property Albumes $idalbum0
 * @property Canciones $idcancion0
 */
class Forman extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcancion', 'idalbum'], 'required'],
            [['idcancion', 'idalbum'], 'integer'],
            [['idalbum', 'idcancion'], 'unique', 'targetAttribute' => ['idalbum', 'idcancion']],
            [['idalbum'], 'exist', 'skipOnError' => true, 'targetClass' => Albumes::className(), 'targetAttribute' => ['idalbum' => 'idalbum']],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::className(), 'targetAttribute' => ['idcancion' => 'idcancion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idforma' => 'Idforma',
            'idcancion' => 'Idcancion',
            'idalbum' => 'Idalbum',
        ];
    }

    /**
     * Gets query for [[Idalbum0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdalbum0()
    {
        return $this->hasOne(Albumes::className(), ['idalbum' => 'idalbum']);
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::className(), ['idcancion' => 'idcancion']);
    }
}
