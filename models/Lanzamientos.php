<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lanzamientos".
 *
 * @property int $idlanzamiento
 * @property int $idartista
 * @property int $idcancion
 *
 * @property Artistas $idartista0
 * @property Canciones $idcancion0
 */
class Lanzamientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lanzamientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idartista', 'idcancion'], 'required'],
            [['idartista', 'idcancion'], 'integer'],
            [['idartista', 'idcancion'], 'unique', 'targetAttribute' => ['idartista', 'idcancion']],
            [['idartista'], 'exist', 'skipOnError' => true, 'targetClass' => Artistas::className(), 'targetAttribute' => ['idartista' => 'idartista']],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::className(), 'targetAttribute' => ['idcancion' => 'idcancion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlanzamiento' => 'Idlanzamiento',
            'idartista' => 'Idartista',
            'idcancion' => 'Idcancion',
        ];
    }

    /**
     * Gets query for [[Idartista0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdartista0()
    {
        return $this->hasOne(Artistas::className(), ['idartista' => 'idartista']);
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::className(), ['idcancion' => 'idcancion']);
    }
}
