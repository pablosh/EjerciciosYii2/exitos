<?php
use yii\helpers\Html;
?>

<div class="card text-center">
    <div class="card-body">
        
        <div class="caption">
            <h2><?= $model->nombre ?></h2>
            <h4> <?= $model->reproducciones ?> Visualizaciones</h4>
            <hr class="my4">
            <p>Álbum: <?= $model->nomalbum ?></p>
            <p>Grupo: <?= $model->nomgrupo ?></p>
            <p>Artistas: <?= $model->nomartistas ?></p>
        </div>

    </div>
</div> 