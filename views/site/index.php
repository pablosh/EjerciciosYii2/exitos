<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use yii\widgets\ListView;
$titulo = "Lista de éxitos";
$this->title = $titulo;
?>

<div class="well well-sm"><h2 style="text-align: center; max-height: 80px"><?=$titulo?></h2></div>
<div class="row">
    
    <div class="col-md-2"></div>
<div class="col-md-8">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_exito',
    ]);
    ?>
</div>
    
</div>